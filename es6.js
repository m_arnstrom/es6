
// Iterate through arrays
// It also works on strings, treating the string as a sequence of Unicode characters
// unlike forEach(), it works with break, continue, and return
for (var value of myArray) {
  console.log(value);
}

// Since ES5, you can use the built-in forEach method:
// ONE MINOR DRAWBACK: you can’t break out of this loop using a break statement or return from the enclosing function using a return statement.
myArray.forEach(function (value) {
  console.log(value);
});

// Iterate through objects 
// for–in was designed to work on plain old Objects with string keys. For Arrays, it’s not so great.
for (var index in myObj) {
  console.log(myObj[index]);
}

